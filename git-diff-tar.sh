#create an archive of all files between two SHA commits, suppressing deleted file warnings
git diff --name-only SHA1..SHA2 --diff-filter=ACMRTUXB | xargs tar -zcf update.tar.gz